describe http('http://127.0.0.1:9108/metrics') do
  its('status') { should eq 200 }
  its('body') { should match /elasticsearch_cluster_health_up 1/ }
  its('body') { should match /elasticsearch_cluster_health_status{cluster="elasticsearch",color="green"} 1/ }
end
