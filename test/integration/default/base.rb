describe user('elasticsearch_exporter') do
  it { should exist }
end

describe group('elasticsearch_exporter') do
  it { should exist }
end

describe file('/usr/bin/elasticsearch_exporter') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
end

describe file('/opt/elasticsearch_exporter/dist') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe service('elasticsearch_exporter') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
