{% from slspath+"/map.jinja" import elasticsearch_exporter with context %}

elasticsearch_exporter-install-service:
  file.managed:
    - name: /etc/systemd/system/elasticsearch_exporter.service
    - source:  salt://{{ slspath }}/files/exporter.service.j2
    - template: jinja
    - context:
        elasticsearch_exporter: {{ elasticsearch_exporter }}

elasticsearch_exporter-service:
  service.running:
    - name: elasticsearch_exporter
    - enable: True
    - start: True
    - watch:
      - file: elasticsearch_exporter-install-service
